//
//  MainColorViewController.swift
//  coloringView
//
//  Created by Maksim on 25.02.2022.
//


import UIKit

class MainColorViewController: UIViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let colorVC = segue.destination as! ViewController
        colorVC.delegate = self
        colorVC.mainViewColor = view.backgroundColor
    }
}

// MARK: - ColorDelegate
extension MainColorViewController: ViewControllerDelegate {
    func setColor(_ color: UIColor) {
        view.backgroundColor = color
    }
}

