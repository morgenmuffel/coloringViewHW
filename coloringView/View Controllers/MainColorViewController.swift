//
//  MainColorViewController.swift
//  coloringView
//
//  Created by Maksim on 19.10.2023.
//

import UIKit

final class MainColorViewController: UIViewController {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let colorVC = segue.destination as? ViewController else { return }
        colorVC.delegate = self
        colorVC.mainViewColor = view.backgroundColor
    }
}

extension MainColorViewController: ViewControllerDelegate {
    func setColor(_ color: UIColor) {
        view.backgroundColor = color
    }
}
